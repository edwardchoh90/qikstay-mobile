
/*
  Generated class for the PromotionsProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/

export class PromotionsProvider {

  foodPromotions;
  roomPromotions;

  foodPromotionJSON;
  roomPromotionJSON;

  constructor() {
    this.foodPromotionJSON = '[{"image":"assets/img/foodPromo/Promo1.jpg","promoName":"$1 Opening Promo", "date": "Date: 20 Jul 17 to 31 Jul 18","details":" \\n $1 Opening Specials at Rumah Rasa \\n Choice of Sate Lilit (Balinese Satay) / Soto Madura"},{"image":"assets/img/foodPromo/Promo2.jpg","promoName":"Asian Weekend Buffet Lunch","date": "20 Jul 17 to 31 Jul 18","details":" \\n  What makes Asian Weekends irresistible? \\n The tantalizing spread featuring must-try dishes such as mutton "}]'
    this.roomPromotionJSON = '[{"image":"assets/img/roomPromo/roomPromo1.jpg","promoName":"3 Days 2 Nights City Tour" , "validity": "20 Jul 17 to 31 Dec 17","promoPeriod":"01 Jul 17 to 31 Dec 18", "minStay":"1", "minCount":"1", "details":" Too close for comfort? That’s why Bay Hotel Singapore wants to plant a bigger surprise at every destination you go in Singapore’s integrated resort while being kindest to your pocket!"},' +
                              '{"image":"assets/img/roomPromo/roomPromo2.jpg","promoName":"Super Deal"                , "validity": "19 Jun 17 to 31 Sept 17","promoPeriod":"01 Aug 17 to 31 Sept 18", "minStay":"1", "minCount":"1", "details":"Get our Super Deal until September 2017. Terms & Conditions apply"}]'
    this.foodPromotions = JSON.parse(this.foodPromotionJSON);
    this.roomPromotions = JSON.parse(this.roomPromotionJSON);

  }

}
