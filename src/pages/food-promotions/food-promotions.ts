import { Component } from '@angular/core';
import {  NavController, NavParams, PopoverController, ModalController } from 'ionic-angular';
import { popUpMenu } from '../../components/popUpMenu'
import { PromotionsProvider } from '../../providers/promotions/promotions'
import { ViewMoreComponent} from "../../components/view-more/view-more";

/**
 * Generated class for the FoodPromotionsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  selector: 'page-food-promotions',
  templateUrl: 'food-promotions.html',
})
export class FoodPromotionsPage {

  foodPromoLists;

  constructor(public navCtrl: NavController, public navParams: NavParams, public popOver:PopoverController,
              public foodPromotion: PromotionsProvider, public modal: ModalController) {

    this.foodPromoLists = this.foodPromotion.foodPromotions;
    //replace \n with li
    for (var i in this.foodPromoLists){
      this.foodPromoLists[i].details = this.foodPromoLists[i].details.replace(/(?:\\n\n|\r|\n)/g, '<li>');
    }

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FoodPromotionsPage');

  }

  menuPopover(event){
    let popover = this.popOver.create(popUpMenu);
    popover.present({
      ev:event
    })}

    foodPromoDetail(foodPromo){
    this.modal.create(ViewMoreComponent,{fnbDetail:foodPromo}).present()
    }

}
