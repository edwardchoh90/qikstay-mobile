import { Component } from '@angular/core';
import { NavController, NavParams, PopoverController, ModalController } from 'ionic-angular';
import { popUpMenu } from '../../components/popUpMenu'
import { PromotionsProvider } from '../../providers/promotions/promotions'
import { ViewMoreComponent} from "../../components/view-more/view-more";

/**
 * Generated class for the RoomPromotionsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  selector: 'page-room-promotions',
  templateUrl: 'room-promotions.html',
})
export class RoomPromotionsPage {
  roomPromoList
  constructor(public navCtrl: NavController, public navParams: NavParams,public modal:ModalController,
              public popOver: PopoverController, public roomPromotion:PromotionsProvider) {

    this.roomPromoList = this.roomPromotion.roomPromotions;

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RoomPromotionsPage');
  }

  menuPopover(event){
    let popover = this.popOver.create(popUpMenu);
    popover.present({
      ev:event
    })}

  roomPromotionDetail(roomPromo){
    this.modal.create(ViewMoreComponent,{roomPromoDetail:roomPromo}).present()
  }





}
