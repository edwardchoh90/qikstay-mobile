import { Component } from '@angular/core';
import { NavController, PopoverController, AlertController, LoadingController } from 'ionic-angular';
import { popUpMenu } from '../../components/popUpMenu'
import { GlobalThemeSettingProvider } from "../../providers/global-theme-setting/global-theme-setting"
import { DatePicker } from '@ionic-native/date-picker';
import Moment from 'moment'

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  colorScheme = this.theme.navBar;
  nightStay:number =1 ;
  childCount = 1;
  childMaxCount = 5;
  adultCount = 1;
  adultMaxCount = 5;
  startDate;
  checkInDate;
  checkOutDate;



  slides = [
    { imgUrl: "assets/img/wallpaper/wallpaper1.jpg"},
    { imgUrl: "assets/img/wallpaper/wallpaper2.jpg"},
    { imgUrl: "assets/img/wallpaper/wallpaper3.jpg"},
    { imgUrl: "assets/img/wallpaper/wallpaper4.jpg"},
  ]

  constructor(public navCtrl: NavController, public popOver: PopoverController,public altCtrl: AlertController,
              public theme:GlobalThemeSettingProvider, public datePicker: DatePicker, public loading: LoadingController) {

    this.checkInDate = Moment()
    this.checkOutDate = Moment().add(1, 'days')

  }

  menuPopover(event){
    let popover = this.popOver.create(popUpMenu);
    popover.present({
      ev:event
    })
  }

  selectOccupantAmount(type){

    let alert = this.altCtrl.create({title: type});
    var maxCount = 0;

    if (type == "Children"){
      maxCount = this.childMaxCount
    }

    if (type == "Adult"){
      maxCount = this.adultMaxCount
    }

    for (var i = 1; i < maxCount; i++) {
      alert.addInput({
        type: 'radio',
        label: i.toString(),
        value: i.toString(),
      });
    }
    alert.addButton('Cancel');
    alert.addButton({
      text: 'OK',
      handler: data => {

        if (type == "Children"){
          this.childCount = data;
        }

        if (type == "Adult"){
          this.adultCount = data;
        }


      }
    });
    alert.present();
  }

  pickDate(orderType){

    //orderType either checkInDate or checkOutDate

    if (orderType == "checkInDate") {

      this.datePicker.show({
        date: new Date(),
        mode: 'date',
        androidTheme: this.datePicker.ANDROID_THEMES.THEME_DEVICE_DEFAULT_LIGHT,
        allowOldDates: false,
        minDate:Moment().toDate()

      }).then(
        date => {
          this.checkInDate = Moment(date)
          this.checkOutDate = this.checkInDate.add(this.nightStay,'days');
        },
        err => {
          console.log('Error occurred while getting date: ', err)
        }
      );
    }

    if (orderType == "checkOutDate") {

      this.datePicker.show({
        date: new Date(),
        mode: 'date',
        androidTheme: this.datePicker.ANDROID_THEMES.THEME_DEVICE_DEFAULT_LIGHT,
        allowOldDates: false,
        minDate:Moment().add(1, 'days').toDate()

      }).then(
        date => {
          console.log('Got date: ', date);
          this.checkOutDate =  Moment(date)
            this.nightStay = this.checkOutDate.diff(this.checkInDate, 'days');
          //backCalculate how many Nights

        },
        err => {
          console.log('Error occurred while getting date: ', err)
        }
      );
    }


  }

  updateCheckOutDate(){
    console.log(this.checkInDate)

    var checkInDate = Moment(this.checkInDate, ["DD-MM-YYYY"]);
    this.checkOutDate =Moment(checkInDate).add(this.nightStay,'days')

    console.log("CheckOutDate",this.checkOutDate)
  }

  confirmBooking(){
    var confirmStatus = false;

    let alert = this.altCtrl.create({
      title: 'Confirm Booking',
      cssClass: 'confirmation',
      message: '<div style="background-color: #4B4FB0"><b>Your booking detail is as below</b>' +
      '<p> Check In Date:' + this.checkInDate.format("DD-MM-YYYY")  + '</p>'+
      '<p> Check Out Date:' + this.checkOutDate.format("DD-MM-YYYY") + '</p>' +
      '<p> Night:'  + this.nightStay  +
      '<span>&nbspAdult:' + this.adultCount  + '&nbspChild:' + this.childCount + '</span>' + '</p></div>'

      ,

      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Confirm',
          handler: ()=>{
            alert.onDidDismiss(data => {
              let loadingMsg = this.loading.create({
                content: "Now Booking",
                duration: 1000,
              })

              loadingMsg.present();
              loadingMsg.onDidDismiss(data2 => {
                this.altCtrl.create({
                  title: "Booking Confirmed",
                  message: "Thank you for staying with us",
                  buttons: ["Confirm"]
                }).present()
              })
            })
          }

        }
      ]
    });
    alert.present();








  }


}
