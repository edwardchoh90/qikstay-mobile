import { Component } from '@angular/core';
import { NavController, NavParams, PopoverController, ModalController } from 'ionic-angular';
import { popUpMenu } from '../../components/popUpMenu'
import { EventsProvider}  from '../../providers/events/events'
import { SearchEvent} from '../events/eventSearch/searchEvent'
import { ViewMoreComponent} from "../../components/view-more/view-more";

/**
 * Generated class for the EventsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-events',
  templateUrl: 'events.html',
})
export class EventsPage {

  eventsList;

  constructor(public navCtrl: NavController, public navParams: NavParams, public popOver: PopoverController,
              public eventService:EventsProvider, public modal:ModalController) {

  }

  ionViewWillLoad() {
   this.eventsList = this.eventService.events;
   console.log(this.eventsList);
  }

  menuPopover(event){
    let popover = this.popOver.create(popUpMenu);
    popover.present({
      ev:event
    })}

  searchEvent(){
    this.modal.create(SearchEvent).present();
  }

  eventDetail(event){

    let eventView = this.modal.create(ViewMoreComponent,{eventDetails:event})
    eventView.present()


  }



}
