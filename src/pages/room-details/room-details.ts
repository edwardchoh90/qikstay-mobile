import { Component } from '@angular/core';
import {  NavController, NavParams, PopoverController,ModalController } from 'ionic-angular';
import { popUpMenu } from '../../components/popUpMenu'
import { RoomDetailsProvider } from '../../providers/room-details/room-details'
import { ViewMoreComponent} from "../../components/view-more/view-more";

/**
 * Generated class for the RoomDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-room-details',
  templateUrl: 'room-details.html',
})
export class RoomDetailsPage {

  roomLists;

  constructor(public navCtrl: NavController, public navParams: NavParams, public popOver: PopoverController,
              public roomDetails:RoomDetailsProvider, public modal: ModalController) {

    this.roomLists = this.roomDetails.roomDetails

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RoomDetailsPage');
  }

  menuPopover(event){
    let popover = this.popOver.create(popUpMenu);
    popover.present({
      ev:event
    })
  }

  roomDetailView(roomDetail){
    this.modal.create(ViewMoreComponent,{roomDetail:roomDetail}).present()
  }

}
