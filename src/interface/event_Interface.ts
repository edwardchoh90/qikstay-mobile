export interface eventInterface {
  image:string,
  eventName:string,
  date: string,
  venue:string
}
