import { Component } from '@angular/core';


@Component({
  selector:"background-Slides",
  template: `
    <ion-slides autoplay="4000" loop="true" speed="2500">
      <ion-slide *ngFor="let slide of slides" [ngStyle]="{
    'background-image': 'url(' + slide.imgUrl + ')',
    'background-size': 'cover', 'background-repeat': 'no-repeat','background-position-x': '50%'}">
      </ion-slide>
    </ion-slides>
  `,
  styles:[],

})

export class backGroundSlider {

  slides = [
    { imgUrl: "assets/img/wallpaper/wallpaper1.jpg"},
    { imgUrl: "assets/img/wallpaper/wallpaper2.jpg"},
    { imgUrl: "assets/img/wallpaper/wallpaper3.jpg"},
    { imgUrl: "assets/img/wallpaper/wallpaper4.jpg"},
  ]


  constructor() {}

}
