import { Component } from '@angular/core';
import { ViewController,NavController,App } from 'ionic-angular';
import {HomePage} from '../pages/home/home'
import {EventsPage} from '../pages/events/events'
import {FoodPromotionsPage} from '../pages/food-promotions/food-promotions'
import {RoomPromotionsPage} from '../pages/room-promotions/room-promotions'
import {RoomDetailsPage} from "../pages/room-details/room-details";


@Component({
  template: `
    <div class="menuSize">
    <ion-list no-lines>
      <button ion-item  detail-none (click)="goToPage(0)">           <ion-icon name="md-home"></ion-icon> &nbsp;Home</button>
      <button ion-item  detail-none (click)="goToPage(1)">         <ion-icon name="md-calendar"></ion-icon> &nbsp;Events</button>
      <button ion-item  detail-none (click)="goToPage(2)">  <ion-icon name="md-restaurant"></ion-icon> &nbsp;F&B Promotions</button>
      <button ion-item  detail-none (click)="goToPage(3)">  <ion-icon name="ios-cash"></ion-icon> &nbsp;Room Promotions</button>
      <button ion-item  detail-none (click)="goToPage(4)">    <ion-icon name="md-card"></ion-icon> &nbsp;Room Details</button>
    </ion-list>
      <button ion-item  detail-none (click)="close()"><ion-icon name="md-person"></ion-icon> &nbsp;Register </button>
    </div>
  `,
  styles:['.menuSize{} .list-ios{margin: -1px 0 0;} .popover-arrow {display:none}'],

})
export class popUpMenu {

  pages=[ { name:"home", page: HomePage },
          { name:"events", page: EventsPage },
          { name:"foodPromotions", page: FoodPromotionsPage },
          { name:"roomPromotions", page: RoomPromotionsPage },
          { name:"roomDetails", page: RoomDetailsPage },]

  constructor(public viewCtrl: ViewController , public navCtrl: NavController, public appCtrl: App) {

  }

  goToPage(pageIndex){
    this.viewCtrl.dismiss();
    this.appCtrl.getRootNav().setRoot(this.pages[pageIndex].page);
  }



}
