import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import {FaIconComponent} from "../components/fa-icon/fa-icon.component";
import {popUpMenu} from "../components/popUpMenu"
import { backGroundSlider } from "../components/backGroundSlider";

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { EventsPage } from '../pages/events/events';
import { SearchEvent } from '../pages/events/eventSearch/searchEvent';
import { FoodPromotionsPage } from '../pages/food-promotions/food-promotions';
import { RegisterPage } from '../pages/register/register';
import { RoomDetailsPage } from '../pages/room-details/room-details';
import { RoomPromotionsPage } from '../pages/room-promotions/room-promotions';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { GlobalThemeSettingProvider } from '../providers/global-theme-setting/global-theme-setting';
import { EventsProvider } from '../providers/events/events';
import { PromotionsProvider } from '../providers/promotions/promotions';
import { RoomDetailsProvider } from '../providers/room-details/room-details';
import { ViewMoreComponent } from '../components/view-more/view-more'

import { DatePicker } from '@ionic-native/date-picker';




@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListPage,
    FaIconComponent,
    popUpMenu,
    backGroundSlider,
    EventsPage,
    SearchEvent,
    FoodPromotionsPage,
    RegisterPage,
    RoomDetailsPage,
    RoomPromotionsPage,
    ViewMoreComponent,

  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp,{
      menuType: 'overlay',
    }),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListPage,
    popUpMenu,
    backGroundSlider,
    EventsPage,
    SearchEvent,
    FoodPromotionsPage,
    RegisterPage,
    RoomDetailsPage,
    RoomPromotionsPage,
    ViewMoreComponent,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    GlobalThemeSettingProvider,
    EventsProvider,
    PromotionsProvider,
    RoomDetailsProvider,
    DatePicker
  ]
})
export class AppModule {}
